var cardInnerBoxArr = null;

/**
 * @description it will reset all elements of an array with animations
 */
function showCardSort(array = [1, 2, 3, 4, 5, 6, 7, 8, 9]){
	if(cardInnerBoxArr!=null)
	{
		for (var i = 0; i < array.length; i++) {
			cardInnerBoxArr[i].style.opacity = "0";
			cardInnerBoxArr[i].style.transition = "2s";
			cardInnerBoxArr[i].style.opacity = "1";
			cardInnerBoxArr[i].innerHTML = "<span>"+array[i]+"</span>";
			cardInnerBoxArr[i].dataset.color = array[i];
		}
	}
}

/**
 * @description it will shffle all the cards with random indexes by swiping the array elements
 */
function suffleFun() {
	var array = [1, 2, 3, 4, 5, 6, 7, 8, 9];
	var currentIndex = array.length, temporaryValue, randomIndex;
	while (0 !== currentIndex) {
    	randomIndex = Math.floor(Math.random() * currentIndex);
    	currentIndex -= 1;
	    temporaryValue = array[currentIndex];
	    array[currentIndex] = array[randomIndex];
	    array[randomIndex] = temporaryValue;
	}
	showCardSort(array);
}

/**
 * @name onload function
 * @description it will reneder all cards at window.onload event
 */
window.onload = function() {
	cardInnerBoxArr = document.getElementsByClassName('card-inner-box');
}